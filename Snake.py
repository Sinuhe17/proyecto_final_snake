import pygame, sys,random
from pygame.locals import *

AnchoVentana =1000
AltoVentana=600

#COLORES PARA LA INTERFAZ 
WHITE       =   (255,255,255)
GREY        =   (200,200,200)
PINK        =   (198,134,156)
BLACK       =   ( 17, 18, 13)
RED         =   (255,  0,  0)
GREEN       =   (0,  255,  0)
DARKGREEN   =   (0,  155,  0)
DARKGRAY    =   (40,  40, 40)
ORANGE      =   (255,155,111)
BGCOLOR= BLACK

def main():
    pygame.init() #inicializamos los modulos que ocuparemos de pygame
    ventana=pygame.display.set_mode((AnchoVentana,AltoVentana)) #nos regresara una superficie
    pygame.display.set_caption('Juego Snake') #Le agregamos titulo a la ventana
   
    
    
    runGame()
    pygame.display.update()
    
    
def runGame():
     while True: #Ciclo ifinito
        for event in pygame.event.get(): #Para escuchar los eventos 
            #Mas que nada esto es para saber si el usuario quiere salir de la ventana
            if event.type == QUIT: #Presionamos la X de la ventana 
                #pygame.quit() ahora esto esta en terminate()
                terminate()
            elif(event.type == KEYDOWN):
                if(event.key == K_ESCAPE):
                    terminate()
        


# Metodo para detectar que tecla se oprime 
def checkForKeyPress():
    #Con ayuda de la funcion len() optenemos el numero de elementos de un objeto 
    #Obtenemos el valor de pygame.eve... el cual son los estadps de la ventana
    #QUIT por defecto esta en 0 y cuando oprimimos pasa a 1
    if len(pygame.event.get(QUIT)) > 0: 
        terminate()#Ejecutamos el metodo terminate() ++
        #Para las teclas oprimidas 
    keyUpEvents = pygame.event.get(KEYUP)
    if len(keyUpEvents) == 0:
        return None
    if keyUpEvents[0].key == K_ESCAPE: #En caso de que se oprima la tecla Esc
        terminate()
    return keyUpEvents[0].key



#Metodo para cerrar la 
def terminate():
    print('¡HAS PERDIDO!')
    pauseGame()
    pygame.quit()#Cierra el juego
    sys.exit()



#Metodo para pausar el juego 
def pauseGame():
    pausar = True
    while (pausar):
        for event in pygame.event.get():
            if (event.type == KEYDOWN):
                if(event.key == K_SPACE):
                    '''Para poder cerrar el juego, una vez que esta en estado  pause
                    se debe de oprimir la tecla espace '''
                    print("Entro al método pauseGame pausar = False")
                    pausar = False
                
        

if __name__== '__main__':
    main()



